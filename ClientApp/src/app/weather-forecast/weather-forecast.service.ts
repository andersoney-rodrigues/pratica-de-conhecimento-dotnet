import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { AppSettingsService } from "../shareds/appsettings.service";

@Injectable()
export class WeatherForecastService {
    constructor(private http: HttpClient) {
    }
    getAll(): Observable<WeatherForecast[]> {
        return this.http.get<WeatherForecast[]>(environment.baseUrl + 'weatherforecast');
    }
}