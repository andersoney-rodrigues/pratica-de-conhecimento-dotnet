import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Title } from '@angular/platform-browser';
import { AppSettingsService } from '../shareds/appsettings.service';
import { WeatherForecastService } from './weather-forecast.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './weather-forecast.component.html',
  providers: [WeatherForecastService]
})
export class WeatherForecastComponent {
  public forecasts: WeatherForecast[];
  constructor(titleService: Title, private weatherForecastService: WeatherForecastService) {
    this.weatherForecastService.getAll().subscribe(result => {
      this.forecasts = result;
    }, error => console.error(error));
    titleService.setTitle("Fetch data pages - " + environment.defaultTitle);
  }
}


