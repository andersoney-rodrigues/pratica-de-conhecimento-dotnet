import { Component } from '@angular/core';
import { Title  } from '@angular/platform-browser';
import { title } from 'process';
import { AppSettingsService } from '../shareds/appsettings.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
  constructor(private titleService: Title,private appSettingsService: AppSettingsService){
    titleService.setTitle("Initial pages - "+appSettingsService.getSettings().defaultTitle);
  }
}
