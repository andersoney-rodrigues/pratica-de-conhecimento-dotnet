import { Injectable } from '@angular/core';
import { AppSettings } from "./appsettings";

@Injectable()
export class AppSettingsService {
  getSettings(): AppSettings {
    let settings = new AppSettings();
    return settings;
  }
  getBaseUrl(): string {
    return 'https://localhost:5002/';
  }
}
