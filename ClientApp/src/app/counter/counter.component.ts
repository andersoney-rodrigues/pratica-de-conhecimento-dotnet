import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AppSettingsService } from '../shareds/appsettings.service';

@Component({
  selector: 'app-counter-component',
  templateUrl: './counter.component.html'
})
export class CounterComponent {
  constructor(private titleService: Title, private appSettingsService: AppSettingsService){
    titleService.setTitle("Counter pages - "+appSettingsService.getSettings().defaultTitle);
  }
  public currentCount = 0;

  public incrementCounter() {
    this.currentCount++;
  }
}
